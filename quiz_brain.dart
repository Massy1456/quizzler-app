import 'question.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class QuizBrain {
  int _questionNumber = 0;
  bool isFinished = false;

  List<Question> _myQuestions = [
    Question("Does Thaumaturgy create an illusory image?", false),
    Question(
        "Does Bless give an additional d4 on Attacks and Saving Throws?", true),
    Question("Is Grasping Vine a 4th level spell?", true),
    Question("Can Druidcraft create a sound?", false),
    Question("Does Confusion force a Wisdom Saving Throw?", true),
    Question("Does the spell 'Hold Person' restrain the target?", false),
    Question(
        "Does Guiding Bolt have the longest range of all 1st-level spells?",
        true),
  ];

  void reset() {
    _questionNumber = 0;
    isFinished = false;
  }

  void nextQuestion() {
    if (_questionNumber < _myQuestions.length - 1) {
      _questionNumber++;
    } else {
      isFinished = true;
    }
  }

  String getQuestionText() {
    return _myQuestions[_questionNumber].questionText;
  }

  bool getQuestionAnswer() {
    return _myQuestions[_questionNumber].questionAnswer;
  }
}
